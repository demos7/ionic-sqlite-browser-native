import { Injectable } from '@angular/core';
import { SQLite } from '@ionic-native/sqlite/ngx';
import { Platform } from '@ionic/angular';
import { browserDBInstance } from './browser';

declare var window: any;
const SQL_DB_NAME = 'tempos.db';

@Injectable({
  providedIn: 'root'
})
export class SqlProvider {

  dbInstance: any;

  constructor(public sqlite: SQLite, private platform: Platform) {
    this.init();
  }

  async init() {
    if (!this.platform.is('cordova')) {
      const db = window.openDatabase(SQL_DB_NAME, '1.0', 'DEV', 5 * 1024 * 1024);
      this.dbInstance = browserDBInstance(db);
    } else {
    this.platform.ready().then(() => {
      this.dbInstance = this.sqlite.create({
        name: SQL_DB_NAME,
        location: 'default'
      });
    }).catch(error => {
      console.log(error);
    });
    }
  }
}